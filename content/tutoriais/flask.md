---
title: "Limpando ferugem ferramentas com eletrólise"
date: 2021-09-18T23:28:40-03:00
draft: false
---

1. Em um recipiente mistura água com sal ou bicarbonato de sódio (1 colher por litro d'água)

2. Seubmerja a ferramenta na solução

3. Insira um eletrodo no recipiente sem tocar na ferramenta

4. Com uma fonte de 9V a 12V de tensão, conecte o terminal positivo da fonte no eletrodo e o negativo na ferramenta, então ligue a fonte

5. Aguarde aproximadamente 45min a 1 hora e sua ferramenta estará sem ferrugens! (ou com menos ferrugens pelo menos :p)