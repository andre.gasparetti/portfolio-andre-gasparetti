---
title: "Usando papel fotográfico para confecção de PCBs"
date: 2021-09-18T23:27:57-03:00
draft: false
---

Criando um site básico com os temas disponíveis do hugo (OS: windows)

1. Tendo seu circuito desenhado através de software adequado, inverta  a imagem do circuito e imprima-o em papel fotográfico

2. Com a placa de cobre virgem, limpe a superfície com palha de aço e alcool isopropílico para remover sujeiras e oxidações

3. Posicione o papel fotográfico na placa de cobre com a tinta do papel voltada para o cobre da placa, então pressione o papel contra a placa usando uma chapa suficientemente quente (um ferro de passar roupa, por exemplo) durante 2 a 3 minutos

4. Deixe a placa resfriar por um tempo e depois passe água

5. Remova cuidadosamente o papel da placa deixando a tinta na placa.

6. Corroa o cobre exposto com percloreto de ferro

7. Limpe a placa com palha de aço e aproveite sua nova PCB