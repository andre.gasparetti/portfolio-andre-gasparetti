---
title: "Curriculo"
date: 2021-09-18T16:59:34-03:00
draft: false
---

## Nome completo:
André Victor da Fonseca Gasparetti

## Formação:
- Técnico em eletrônica: Escola Técnica Estadual de São Paulo (2018 - 2021)
- Engenharia Mecatrônica: Escola Politécnica da Universidade de São Paulo (2022 - presente)

## Habilidades

- Idiomas extrangeiros:
    - Inglês avançado
    - Alemão básico
    - Espanhol básico

- Linguagens:
    - HTML
    - CSS
    - JavaScript
    - C++
    - Python

- Softwares:
    - Proteus Isis
    - MPLAB
    - Inventor
    - NX
    - Fusion
    - VsCode
    - Autocad
    - Ansys
    - Altium
    - Vivado
    - Fusion
    - Abaqus
    - Cube
    - Octave
    - PIPE

- Pacote Office Intermediário

## Extensão:
- (1 ano) Atuação no Grupo de Foguetemodelismo da Escola Politécnica: Jupiter
- (1 ano) Atuação no Grupo de Robótica Competitiva da Escola Politécnica: ThundeRatz