---
title: "Sobre mim"
date: 2021-09-18T16:59:34-03:00
draft: false
---

Meu nome, como você já deve ter visto, é André Victor da Fonseca Gasparetti e tenho 21 anos. Sou técnico em eletrônica pela ETESP e atualmente curso engenharia mecatrônica pela Escola Politécnica da USP desde 2022.

Tenho interesse em áreas como biomecatrônica, aeronautica, robótica, entre outros. Meus hobbies incluem musculação, pescaria, tocar violino, jogar videogames e ler livros.